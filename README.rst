Test Any Type GeneFlow App
==========================

Version: 0.1

This GeneFlow app tests the functionality of the 'Any' datatype.

Inputs
------

1. input: File or directory

Parameters
----------

1. param: File or directory
 
2. output: Output test file

